package com.esgi.nova.users.exceptions

import java.lang.Exception

class UserNotFoundException : Exception()
